package com.example.evaluation02;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = CarRentalApplicationTests.class)
class CarRentalApplicationTests {

	@Test
	void contextLoads() {
	}

}
