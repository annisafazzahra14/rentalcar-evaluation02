package com.example.evaluation02.service;

import com.example.evaluation02.dtos.car.CarAddDto;
import com.example.evaluation02.dtos.car.CarGetDto;
import com.example.evaluation02.dtos.car.CarPutDto;
import com.example.evaluation02.exception.NonExistanceResourceException;
import com.example.evaluation02.exception.ResourceAlreadyExistException;
import com.example.evaluation02.model.Car;
import com.example.evaluation02.repositories.CarRepository;
import org.modelmapper.Conditions;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class CarServiceImplemet implements CarService{
    @Autowired
    CarRepository carRepository;
    @Autowired
    ModelMapper modelMapper;
    @Override
    public Car addCar(CarAddDto carAddDto) throws ResourceAlreadyExistException {
        Car car = carRepository.findCarByPlate(carAddDto.getPlate());
        if(car != null){
            throw new ResourceAlreadyExistException("car with plate "+car.getPlate()+" already exist");
        }
        return carRepository.save(modelMapper.map(carAddDto, Car.class));
    }

    @Override
    public List<CarGetDto> showAllCar() {
        List<Car> cars = carRepository.findAll();
        return cars.stream().map(i-> modelMapper.map(i, CarGetDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public CarGetDto findById(UUID id) throws NonExistanceResourceException{
        Car car = carRepository.findById(id)
                .orElseThrow(() -> new NonExistanceResourceException("Car with id " + id + " doesnt exist"));
        return modelMapper.map(car, CarGetDto.class);
    }

    @Override
    public Car updateCar(CarPutDto carPutDto, UUID id) throws NonExistanceResourceException {
        Car car = carRepository.findById(id)
                .orElseThrow(() -> new NonExistanceResourceException("Car with id " + id + " doesnt exist"));
        if (carPutDto.getBrand() != null) {
            car.setBrand(carPutDto.getBrand());
        }
        if (carPutDto.getColor() != null) {
            car.setColor(carPutDto.getColor());
        }
        if (carPutDto.getYear() != null) {
            car.setYear(carPutDto.getYear());
        }
        if (carPutDto.getPlate() != null) {
            car.setPlate(carPutDto.getPlate());
        }
        return carRepository.save(car);
    }

    @Override
    public void deleteCar(UUID id) throws NonExistanceResourceException {
        Car car = carRepository.findById(id)
                .orElseThrow(() -> new NonExistanceResourceException("Car with id " + id + " doesnt exist"));
        carRepository.delete(car);
    }
}
