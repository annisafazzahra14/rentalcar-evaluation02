package com.example.evaluation02.service;

import com.example.evaluation02.dtos.admin.AdminAddDto;
import com.example.evaluation02.dtos.admin.AdminGetDto;
import com.example.evaluation02.dtos.admin.AdminPutDto;
import com.example.evaluation02.exception.NonExistanceResourceException;
import com.example.evaluation02.exception.ResourceAlreadyExistException;
import com.example.evaluation02.model.Admin;
import com.example.evaluation02.model.JwtRequest;
import com.example.evaluation02.repositories.AdminRepository;
import com.example.evaluation02.utilities.JwtTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class AdminServiceImplement implements AdminService, UserDetailsService {
    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    JwtTokenUtil jwtTokenUtil;
    @Autowired
    AdminRepository adminRepository;
    @Autowired
    ModelMapper modelMapper;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Override
    public Admin addAdmin(AdminAddDto adminAddDto) throws ResourceAlreadyExistException {
        Admin admin = adminRepository.findAdminByUsername(adminAddDto.getUsername());
        if(admin != null) throw new ResourceAlreadyExistException("Admin with username "+admin.getUsername()+" already exist");
        adminAddDto.setPassword(passwordEncoder.encode(adminAddDto.getPassword()));
        return adminRepository.save(modelMapper.map(adminAddDto, Admin.class));
    }

    @Override
    public List<AdminGetDto> showAllAdmin() {
        List<Admin> admins = adminRepository.findAll();
        return admins.stream()
                .map(i -> modelMapper.map(i, AdminGetDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public AdminGetDto findById(UUID id) throws NonExistanceResourceException {
        Admin admin = adminRepository.findById(id)
                .orElseThrow(()-> new NonExistanceResourceException("Admin with id "+id+" doesnt exist"));
        return modelMapper.map(admin, AdminGetDto.class);
    }

    @Override
    public Admin updateAdmin(AdminPutDto adminPutDto, UUID id) throws NonExistanceResourceException {
        Admin admin = adminRepository.findById(id)
                .orElseThrow(() -> new NonExistanceResourceException("Admin with id " + id + " doesnt exist"));
        if (adminPutDto.getPassword() != null) {
            admin.setPassword(passwordEncoder.encode(adminPutDto.getPassword()));
        }
        if (adminPutDto.getName() != null) {
            admin.setName(adminPutDto.getName());
        }
        if (adminPutDto.getUsername() != null) {
            admin.setUsername(adminPutDto.getUsername());
        }
        return adminRepository.save(admin);
    }

    @Override
    public void deleteAdmin(UUID id) throws NonExistanceResourceException {
            Admin admin = adminRepository.findById(id)
                    .orElseThrow(() -> new NonExistanceResourceException("Admin with id " + id + " doesnt exist"));
        adminRepository.delete(admin);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Admin admin = adminRepository.findAdminByUsername(username);
        if(admin.getUsername().equals(username)){
            return new User(admin.getUsername(),admin.getPassword(), new ArrayList<>());
        } else {
            throw new UsernameNotFoundException("Admin with username "+username+" not found");
        }

    }
    @Override
    public String generateAuthenticateToken(JwtRequest jwtRequest) throws Exception{
        authenticate(jwtRequest.getUsername(), jwtRequest.getPassword());
        final UserDetails userDetails = loadUserByUsername(jwtRequest.getUsername());
        return jwtTokenUtil.generateToken(userDetails);
    }
    private void authenticate(String username, String password) throws Exception{
        Objects.requireNonNull(username);
        Objects.requireNonNull(password);
        try{
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        }
        catch (DisabledException e){
            throw new Exception("User Disabled", e);
        }
        catch (BadCredentialsException e){
            throw new Exception("Invalid Credentials", e);
        }
    }
}
