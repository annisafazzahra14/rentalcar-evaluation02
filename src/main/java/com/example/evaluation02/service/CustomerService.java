package com.example.evaluation02.service;

import com.example.evaluation02.dtos.customer.CustomerAddDto;
import com.example.evaluation02.dtos.customer.CustomerGetDto;
import com.example.evaluation02.dtos.customer.CustomerPutDto;
import com.example.evaluation02.exception.NonExistanceResourceException;
import com.example.evaluation02.exception.ResourceAlreadyExistException;
import com.example.evaluation02.model.Customer;

import java.util.List;
import java.util.UUID;

public interface CustomerService {
    Customer addCustomer(CustomerAddDto customerAddDto) throws ResourceAlreadyExistException;
    List<CustomerGetDto> showAllCustomer();
    CustomerGetDto findById(UUID id) throws NonExistanceResourceException;
    Customer updateCustomer(CustomerPutDto customerPutDto, UUID id) throws NonExistanceResourceException;
    void deleteCustomer(UUID id) throws NonExistanceResourceException;
    void payFineBill(UUID id, Float payout) throws NonExistanceResourceException;
}
