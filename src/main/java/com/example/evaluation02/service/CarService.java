package com.example.evaluation02.service;

import com.example.evaluation02.dtos.car.CarAddDto;
import com.example.evaluation02.dtos.car.CarGetDto;
import com.example.evaluation02.dtos.car.CarPutDto;
import com.example.evaluation02.exception.NonExistanceResourceException;
import com.example.evaluation02.exception.ResourceAlreadyExistException;
import com.example.evaluation02.model.Car;

import java.util.List;
import java.util.UUID;

public interface CarService {
    Car addCar(CarAddDto carAddDto) throws ResourceAlreadyExistException;
    List<CarGetDto> showAllCar();
    CarGetDto findById(UUID id) throws NonExistanceResourceException;
    Car updateCar(CarPutDto carPutDto, UUID id) throws NonExistanceResourceException;
    void deleteCar(UUID id) throws NonExistanceResourceException;
}
