package com.example.evaluation02.service;

import com.example.evaluation02.dtos.transaction.TransactionAddDto;
import com.example.evaluation02.dtos.transaction.TransactionGetDto;
import com.example.evaluation02.dtos.transaction.TransactionPutDto;
import com.example.evaluation02.exception.NonExistanceResourceException;
import com.example.evaluation02.exception.ResourceAlreadyExistException;
import com.example.evaluation02.model.Transaction;

import java.util.List;
import java.util.Map;
import java.util.UUID;

public interface TransactionService {
    Transaction addTransaction(TransactionAddDto transactionAddDto) throws ResourceAlreadyExistException;
    List<TransactionGetDto> showAllTransaction();
    TransactionGetDto findById(UUID id) throws NonExistanceResourceException;
    Transaction updateTransaction(TransactionPutDto transactionPutDto, UUID id) throws NonExistanceResourceException;
    void deleteTransaction(UUID id) throws NonExistanceResourceException;
    Transaction returnCar(UUID id) throws NonExistanceResourceException;
}
