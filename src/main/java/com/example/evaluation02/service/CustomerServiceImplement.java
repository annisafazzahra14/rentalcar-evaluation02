package com.example.evaluation02.service;

import com.example.evaluation02.dtos.customer.CustomerAddDto;
import com.example.evaluation02.dtos.customer.CustomerGetDto;
import com.example.evaluation02.dtos.customer.CustomerPutDto;
import com.example.evaluation02.exception.NonExistanceResourceException;
import com.example.evaluation02.exception.ResourceAlreadyExistException;
import com.example.evaluation02.model.Customer;
import com.example.evaluation02.repositories.CustomerRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class CustomerServiceImplement implements CustomerService{
    @Autowired
    CustomerRepository customerRepository;
    @Autowired
    ModelMapper modelMapper;
    @Override
    public Customer addCustomer(CustomerAddDto customerAddDto) throws ResourceAlreadyExistException {
        Customer customer = customerRepository.findCustomerByIdCard(customerAddDto.getIdCard());
        if(customer != null){
            throw new ResourceAlreadyExistException("Customer with IDCard "+customer.getIdCard()+" already exist");
        }
        return customerRepository.save(modelMapper.map(customerAddDto, Customer.class));
    }

    @Override
    public List<CustomerGetDto> showAllCustomer() {
        List<Customer> customers = customerRepository.findAll();
        return customers.stream().map(i -> modelMapper.map(i, CustomerGetDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public CustomerGetDto findById(UUID id) throws NonExistanceResourceException {
        Customer customer = customerRepository.findById(id)
                .orElseThrow(() -> new NonExistanceResourceException("Customer with id " + id + " doesnt exist"));
        return modelMapper.map(customer, CustomerGetDto.class);
    }

    @Override
    public Customer updateCustomer(CustomerPutDto customerPutDto, UUID id) throws NonExistanceResourceException {
        Customer customer = customerRepository.findById(id)
                .orElseThrow(() -> new NonExistanceResourceException("Customer with id " + id + " doesnt exist"));
        if (customerPutDto.getAddress() != null) {
            customer.setAddress(customerPutDto.getAddress());
        }
        if (customerPutDto.getName() != null) {
            customer.setName(customerPutDto.getName());
        }
        if (customerPutDto.getGender() != null) {
            customer.setGender(customerPutDto.getGender());
        }
        if (customerPutDto.getPhone() != null) {
            customer.setPhone(customerPutDto.getPhone());
        }
        if (customerPutDto.getIdCard() != null) {
            customer.setIdCard(customerPutDto.getIdCard());
        }
        return customerRepository.save(customer);
    }

    @Override
    public void deleteCustomer(UUID id) throws NonExistanceResourceException {
        Customer customer = customerRepository.findById(id)
                .orElseThrow(() -> new NonExistanceResourceException("Customer with id " + id + " doesnt exist"));
        customerRepository.delete(customer);
    }

    @Override
    public void payFineBill(UUID id, Float payout) throws NonExistanceResourceException {
        Customer customer = customerRepository.findById(id)
                .orElseThrow(() -> new NonExistanceResourceException("Customer with id " + id + " doesnt exist"));
        customer.setFineBill(customer.getFineBill()-payout);
        customerRepository.save(customer);
    }
}
