package com.example.evaluation02.service;

import com.example.evaluation02.dtos.transaction.TransactionAddDto;
import com.example.evaluation02.dtos.transaction.TransactionGetDto;
import com.example.evaluation02.dtos.transaction.TransactionPutDto;
import com.example.evaluation02.exception.NonExistanceResourceException;
import com.example.evaluation02.exception.ResourceAlreadyExistException;
import com.example.evaluation02.model.Admin;
import com.example.evaluation02.model.Car;
import com.example.evaluation02.model.Customer;
import com.example.evaluation02.model.Transaction;
import com.example.evaluation02.repositories.AdminRepository;
import com.example.evaluation02.repositories.CarRepository;
import com.example.evaluation02.repositories.CustomerRepository;
import com.example.evaluation02.repositories.TransactionRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

import static java.time.temporal.ChronoUnit.DAYS;

@Service
public class TransactionServiceImplement implements TransactionService{
    @Autowired
    TransactionRepository transactionRepository;
    @Autowired
    CarRepository carRepository;
    @Autowired
    CustomerRepository customerRepository;
    @Autowired
    AdminRepository adminRepository;
    @Autowired
    ModelMapper modelMapper;
    @Override
    public Transaction addTransaction(TransactionAddDto transactionAddDto) throws ResourceAlreadyExistException, IllegalArgumentException {
        Admin admin = adminRepository.findById(transactionAddDto.getAdmin()).get();
        Car car = carRepository.findById(transactionAddDto.getCar()).get();
        Customer customer = customerRepository.findById(transactionAddDto.getCustomer()).get();
        Transaction transaction = modelMapper.map(transactionAddDto, Transaction.class);
        if ((transaction != null) && (admin != null) && (car != null) && (customer != null) && (car.getStatus().contains("not"))) {
            transaction.setAdmin(admin);
            transaction.setCar(car);
            transaction.setCustomer(customer);
            transaction.setTransactionTime(LocalDate.now());

            setPrice(transaction);
            setStatusCarAndTransaction(transaction);
            setTotalPayment(transaction);
        } else throw new IllegalArgumentException("please enter valid data");
        return transactionRepository.save(transaction);
    }
    @Override
    public List<TransactionGetDto> showAllTransaction() {
        List<Transaction> transactions = transactionRepository.findAll();
        //update perubahan status
        for(Transaction i : transactions){
            setStatusCarAndTransaction(i);
        }
        return transactions.stream()
                .map(i -> modelMapper.map(i, TransactionGetDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public TransactionGetDto findById(UUID id) throws NonExistanceResourceException {
        Transaction transaction = transactionRepository.findById(id)
                .orElseThrow(() -> new NonExistanceResourceException("Transaction with id " + id + " doesnt exist"));
        setStatusCarAndTransaction(transaction);
        return modelMapper.map(transaction, TransactionGetDto.class);
    }

    @Override
    public Transaction updateTransaction(TransactionPutDto transactionPutDto, UUID id) throws NonExistanceResourceException {
        Transaction transaction = transactionRepository.findById(id)
                .orElseThrow(() -> new NonExistanceResourceException("Transaction with id " + id + " doesnt exist"));
        if(transactionPutDto.getBorrowDate() != null){
            transaction.setBorrowDate(transactionPutDto.getBorrowDate());
        }
        if (transactionPutDto.getDueDate() != null) {
            transaction.setDueDate(transactionPutDto.getDueDate());
        }
        if (transactionPutDto.getReturnDate() != null && transaction.getReturnDate() != null) {
            transaction.setReturnDate(transactionPutDto.getReturnDate());
        } else if(transaction.getReturnDate() == null && transactionPutDto.getReturnDate() != null) {
            throw new IllegalArgumentException("Car hasnt been returned");
        }
        if((transactionPutDto.getBorrowDate() != null) || (transactionPutDto.getReturnDate() != null)  || (transactionPutDto.getDueDate() != null)){
            Float paymentBefore = transaction.getTotalPayment();
            //update denda customer
            if(transaction.getReturnDate() != null ) {
                if(transaction.getReturnDate().isAfter(transaction.getDueDate())) {
                    setTotalFine(transaction);
                } else transaction.setTotalFine(0F);
            }
            setPrice(transaction);
            setTotalPayment(transaction);
            //update tagihan customer
            Float paymentNow = transaction.getTotalPayment();
            if (paymentNow != paymentBefore){
                Float priceDifference = paymentNow-paymentBefore;
                transaction.getCustomer().setFineBill(transaction.getCustomer().getFineBill()+priceDifference);
            }
        }
        if (transactionPutDto.getCar() != null) {
            transaction.getCar().setStatus("not rented");
            Car car = carRepository.findById(transactionPutDto.getCar()).get();
            transaction.setCar(car);
            setStatusCarAndTransaction(transaction);
        }
        if (transactionPutDto.getAdmin() != null) {
            Admin admin = adminRepository.findById(transactionPutDto.getAdmin()).get();
            transaction.setAdmin(admin);
        }
        if (transactionPutDto.getCustomer() != null) {
            Customer customerBefore = transaction.getCustomer();
            Customer customer = customerRepository.findById(transactionPutDto.getCustomer()).get();
            transaction.setCustomer(customer);
            //bila customer sebelumnya terkena tagihan
            if (transaction.getReturnDate() != null) {
                customerBefore.setFineBill(customerBefore.getFineBill() - transaction.getTotalPayment());
                customer.setFineBill(customer.getFineBill() + transaction.getTotalPayment());
            }
        }
        setStatusCarAndTransaction(transaction);
        return transactionRepository.save(transaction);
    }

    @Override
    public void deleteTransaction(UUID id) throws NonExistanceResourceException {
        Transaction transaction = transactionRepository.findById(id)
                .orElseThrow(() -> new NonExistanceResourceException("Transaction with id " + id + " doesnt exist"));
        transaction.getCar().setStatus("not rented");
        transactionRepository.delete(transaction);
    }

    @Override
    public Transaction returnCar(UUID id) throws NonExistanceResourceException {
        Transaction transaction = transactionRepository.findById(id)
                .orElseThrow(() -> new NonExistanceResourceException("Transaction with id " + id + " doesnt exist"));
        transaction.setReturnDate(LocalDate.now());
        setStatusCarAndTransaction(transaction);
        //hitungan denda
        if (transaction.getReturnDate().isAfter(transaction.getDueDate())){
            setTotalFine(transaction);
        }
        //set tagihan
        setTotalPayment(transaction);
        Float customerPreviousBill = transaction.getCustomer().getFineBill();
        transaction.getCustomer().setFineBill(customerPreviousBill+transaction.getTotalPayment());
        return transactionRepository.save(transaction);
    }
    public void setTotalFine(Transaction transaction){
        long fineTime =  DAYS.between(transaction.getDueDate(), transaction.getReturnDate());
        transaction.setTotalFine(transaction.getDailyFine()*fineTime);
    }
    public void setPrice(Transaction transaction){
        long rentalTime = DAYS.between(transaction.getBorrowDate(), transaction.getDueDate());
        transaction.setPrice(400000F*rentalTime);
    }
    public void setTotalPayment(Transaction transaction){
        if (transaction.getTotalFine() != 0) {
            transaction.setTotalPayment(transaction.getPrice() + transaction.getTotalFine());
        } else transaction.setTotalPayment(transaction.getPrice());
    }
    public void setStatusCarAndTransaction(Transaction transaction){
        if (transaction.getReturnDate() == null) {
            if (LocalDate.now().isBefore(transaction.getBorrowDate())) {
                transaction.setStatus("booking");
                transaction.getCar().setStatus("booked");
            } else {
                transaction.setStatus("being rented");
                transaction.getCar().setStatus("rented");
            }
        } else {
            transaction.setStatus("returned");
            transaction.getCar().setStatus("not rented");
        }
    }
}
