package com.example.evaluation02.service;

import com.example.evaluation02.dtos.admin.AdminAddDto;
import com.example.evaluation02.dtos.admin.AdminGetDto;
import com.example.evaluation02.dtos.admin.AdminPutDto;
import com.example.evaluation02.exception.NonExistanceResourceException;
import com.example.evaluation02.exception.ResourceAlreadyExistException;
import com.example.evaluation02.model.Admin;
import com.example.evaluation02.model.JwtRequest;

import java.util.List;
import java.util.UUID;

public interface AdminService {
    Admin addAdmin(AdminAddDto adminAddDto) throws ResourceAlreadyExistException;
    List<AdminGetDto> showAllAdmin();
    AdminGetDto findById(UUID id) throws NonExistanceResourceException;
    Admin updateAdmin(AdminPutDto adminPutDto, UUID id) throws NonExistanceResourceException;
    void deleteAdmin(UUID id) throws NonExistanceResourceException;
    String generateAuthenticateToken(JwtRequest jwtRequest) throws Exception;
}
