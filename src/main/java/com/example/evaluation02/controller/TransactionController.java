package com.example.evaluation02.controller;

import com.example.evaluation02.dtos.transaction.TransactionAddDto;
import com.example.evaluation02.dtos.transaction.TransactionPutDto;
import com.example.evaluation02.exception.NonExistanceResourceException;
import com.example.evaluation02.exception.ResourceAlreadyExistException;
import com.example.evaluation02.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.UUID;


@CrossOrigin
@RestController
@RequestMapping(value = "/api/transaction")
public class TransactionController {
    @Autowired
    TransactionService transactionService;
    @PostMapping(value = "/add")
    public ResponseEntity<Object>  addTransaction(@RequestBody TransactionAddDto transactionAddDto) throws ResourceAlreadyExistException{
        return ResponseEntity.ok(transactionService.addTransaction(transactionAddDto));
    }
    @GetMapping(value = "/showAll")
    public ResponseEntity<Object> showAllCar() {
        return ResponseEntity.ok(transactionService.showAllTransaction());
    }
    @GetMapping(value = "/showById/{id}")
    public ResponseEntity<Object> showById(@PathVariable UUID id) throws NonExistanceResourceException{
        return ResponseEntity.ok(transactionService.findById(id));
    }
    @PutMapping(value = "/update/{id}")
    public ResponseEntity<Object> updateTransaction(@RequestBody TransactionPutDto transactionPutDto, @PathVariable UUID id) throws NonExistanceResourceException{
        return ResponseEntity.ok(transactionService.updateTransaction(transactionPutDto, id));
    }
    @DeleteMapping(value = "delete/{id}")
    public ResponseEntity<String> deleteTransaction(@PathVariable UUID id)throws NonExistanceResourceException{
        transactionService.deleteTransaction(id);
        return ResponseEntity.ok("Transaction with ID "+id+" successfully deleted");
    }
    @PutMapping(value = "/returnCar/{id}")
    public ResponseEntity<Object> returnCar(@PathVariable UUID id) throws NonExistanceResourceException{
        return ResponseEntity.ok(transactionService.returnCar(id));
    }
}
