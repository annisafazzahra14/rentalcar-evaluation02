package com.example.evaluation02.controller;

import com.example.evaluation02.dtos.customer.CustomerAddDto;
import com.example.evaluation02.dtos.customer.CustomerPutDto;
import com.example.evaluation02.exception.NonExistanceResourceException;
import com.example.evaluation02.exception.ResourceAlreadyExistException;
import com.example.evaluation02.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.xml.ws.Response;
import java.util.UUID;

@CrossOrigin
@RestController
@RequestMapping(value = "/api/customer")
public class CustomerController {
    @Autowired
    CustomerService customerService;
    @PostMapping(value = "/add")
    public ResponseEntity<Object> addCustomer(@RequestBody CustomerAddDto customerAddDto) throws ResourceAlreadyExistException{
        return ResponseEntity.ok(customerService.addCustomer(customerAddDto));
    }
    @GetMapping(value = "/showAll")
    public ResponseEntity<Object> showAllCustomer() throws NonExistanceResourceException{
        return ResponseEntity.ok(customerService.showAllCustomer());
    }
    @GetMapping(value = "/showById/{id}")
    public ResponseEntity<Object> showById(@PathVariable UUID id) throws NonExistanceResourceException{
        return ResponseEntity.ok(customerService.findById(id));
    }
    @PutMapping(value = "/update/{id}")
    public ResponseEntity<Object> updateCustomer(@RequestBody CustomerPutDto customerPutDto, @PathVariable UUID id) throws NonExistanceResourceException{
        return ResponseEntity.ok(customerService.updateCustomer(customerPutDto, id));
    }
    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity<String> deleteCustomer(@PathVariable UUID id) throws NonExistanceResourceException{
        customerService.deleteCustomer(id);
        return ResponseEntity.ok("Customer with id "+id+" successfully deleted");
    }
    @PutMapping(value = "/payout/{id}")
    public ResponseEntity<String> payoutCustomer(@PathVariable UUID id, @RequestParam Float payout) throws NonExistanceResourceException{
        customerService.payFineBill(id, payout);
        return ResponseEntity.ok("Payment from customer with id "+id+" success!");
    }
}
