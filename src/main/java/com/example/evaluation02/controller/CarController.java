package com.example.evaluation02.controller;

import com.example.evaluation02.dtos.car.CarAddDto;
import com.example.evaluation02.dtos.car.CarPutDto;
import com.example.evaluation02.exception.NonExistanceResourceException;
import com.example.evaluation02.exception.ResourceAlreadyExistException;
import com.example.evaluation02.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.parameters.P;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.ManyToOne;
import javax.xml.ws.Response;
import java.text.BreakIterator;
import java.util.Objects;
import java.util.UUID;

@CrossOrigin
@RestController
@RequestMapping(value = "/api/car")
public class CarController {
    @Autowired
    CarService carService;
    @PostMapping(value = "/add")
    public ResponseEntity<Object> addCar(@RequestBody CarAddDto carAddDto) throws ResourceAlreadyExistException{
        try {
            Objects.requireNonNull(carAddDto.getBrand());
        } catch (NullPointerException e){
            throw new NullPointerException("Please enter valid data");
        }
        return ResponseEntity.ok(carService.addCar(carAddDto));
    }
    @GetMapping(value = "/showAll")
    public ResponseEntity<Object> showAllCar(){
        return ResponseEntity.ok(carService.showAllCar());
    }
    @GetMapping(value = "/showById/{id}")
    public ResponseEntity<Object> showById(@PathVariable UUID id) throws NonExistanceResourceException{
           return ResponseEntity.ok(carService.findById(id));
    }
    @PutMapping(value = "/update/{id}")
    public ResponseEntity<Object> updateCar(@RequestBody CarPutDto carPutDto, @PathVariable UUID id) throws NonExistanceResourceException{
        return ResponseEntity.ok(carService.updateCar(carPutDto, id));
    }
    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity<String> deleteCar(@PathVariable UUID id) throws NonExistanceResourceException{
        carService.deleteCar(id);
        return ResponseEntity.ok().body("Car with id "+id+" succesfully deleted");
    }
}
