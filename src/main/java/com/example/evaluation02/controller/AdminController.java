package com.example.evaluation02.controller;

import com.example.evaluation02.dtos.admin.AdminAddDto;
import com.example.evaluation02.dtos.admin.AdminPutDto;
import com.example.evaluation02.exception.NonExistanceResourceException;
import com.example.evaluation02.exception.ResourceAlreadyExistException;
import com.example.evaluation02.model.Admin;
import com.example.evaluation02.model.JwtRequest;
import com.example.evaluation02.model.JwtResponse;
import com.example.evaluation02.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;


@CrossOrigin
@RestController
@RequestMapping(value = "/api/admin")
public class AdminController {
    @Autowired
    AdminService adminService;
    @PostMapping(value = "/register")
    public ResponseEntity<Object> registerAdmin(@RequestBody AdminAddDto adminAddDto) throws ResourceAlreadyExistException {
        Admin admin  = adminService.addAdmin(adminAddDto);
        return ResponseEntity.ok(admin);
    }
    @PostMapping(value = "/login")
    public ResponseEntity<?> generateAuthenticateToken(@RequestBody JwtRequest jwtRequest)throws Exception{
        String token = adminService.generateAuthenticateToken(jwtRequest);
        return ResponseEntity.ok(new JwtResponse(token));
    }

    @GetMapping(value = "/showAll")
    public ResponseEntity<Object> showAllAdmin() {
        return ResponseEntity.ok(adminService.showAllAdmin());
    }
    @GetMapping(value = "/showById/{id}")
    public ResponseEntity<Object> showById(@PathVariable UUID id) throws NonExistanceResourceException{
        return ResponseEntity.ok(adminService.findById(id));
    }
    @PutMapping(value = "/update/{id}")
    public ResponseEntity<Object> updateAdmin(@RequestBody AdminPutDto adminPutDto, @PathVariable UUID id) throws NonExistanceResourceException{
        return ResponseEntity.ok(adminService.updateAdmin(adminPutDto, id));
    }
    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity<String> deleteAdmin(@PathVariable UUID id) throws NonExistanceResourceException{
        adminService.deleteAdmin(id);
        return ResponseEntity.ok("Admin with id "+id+" successfully deleted.");
    }
}
