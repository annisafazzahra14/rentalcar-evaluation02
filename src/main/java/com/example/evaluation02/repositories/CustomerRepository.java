package com.example.evaluation02.repositories;

import com.example.evaluation02.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, UUID>{
    Customer findCustomerByIdCard(String idCard);
}
