package com.example.evaluation02.repositories;

import com.example.evaluation02.model.Car;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface CarRepository extends JpaRepository<Car, UUID> {
    Car findCarByPlate(String plate);
}
