package com.example.evaluation02.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;
@Entity
@Data
@Table(name = "car")
public class Car {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private UUID id;

    @Column(name = "brand")
    private String brand;

    @Column(name = "plat")
    private String plate;

    @Column(name = "color")
    private String color;

    @Column(name = "year")
    private Integer year;

    @Column(name = "status")
    private String status;

    @JsonIgnore
    @OneToMany(mappedBy = "car", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Transaction> transaction;
}
