package com.example.evaluation02.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class SwaggerUIConfiguration implements WebMvcConfigurer {
    //untuk custom path swagger
    @Override
    public void addViewControllers(ViewControllerRegistry registry){
     registry.addRedirectViewController("/rentalcar", "/swagger-ui.html");
    }
}
