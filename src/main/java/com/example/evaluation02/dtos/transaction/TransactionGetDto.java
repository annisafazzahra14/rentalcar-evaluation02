package com.example.evaluation02.dtos.transaction;

import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
public class TransactionGetDto {
    private UUID id;

    private UUID idAdmin;
    private String nameAdmin;
    private String usernameAdmin;

    private UUID idCar;
    private String brandCar;
    private String plateCar;

    private UUID idCustomer;
    private String nameCustomer;
    private String addressCustomer;

    private Float price;
    private Float dailyFine;
    private Float totalFine;
    private String status;
    private Float totalPayment;

    private LocalDate transactionDate;
    private LocalDate borrowDate;
    private LocalDate dueDate;
    private LocalDate returnDate;
}
