package com.example.evaluation02.dtos.transaction;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.UUID;

@Data
public class TransactionPutDto {
    private UUID admin;
    private UUID car;
    private UUID customer;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate returnDate;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate borrowDate;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate dueDate;
}
