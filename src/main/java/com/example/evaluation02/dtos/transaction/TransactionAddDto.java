package com.example.evaluation02.dtos.transaction;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.UUID;

@Data
public class TransactionAddDto {
    private UUID customer;
    private UUID car;
    private UUID admin;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate borrowDate;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate dueDate;
}
