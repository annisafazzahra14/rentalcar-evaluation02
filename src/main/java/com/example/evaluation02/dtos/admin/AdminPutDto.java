package com.example.evaluation02.dtos.admin;

import lombok.Data;

@Data
public class AdminPutDto {
    private String name;
    private String username;
    private String password;
}
