package com.example.evaluation02.dtos.admin;

import lombok.Data;

import java.util.UUID;

@Data
public class AdminGetDto {
    private UUID id;
    private String name;
    private String username;
}
