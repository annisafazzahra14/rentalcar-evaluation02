package com.example.evaluation02.dtos.admin;

import lombok.Data;

@Data
public class AdminAddDto {
    private String name;
    private String username;
    private String password;
}
