package com.example.evaluation02.dtos.customer;

import lombok.Data;

@Data
public class CustomerPutDto {
    private String name;
    private String address;
    private String gender;
    private String phone;
    private String idCard;
}
