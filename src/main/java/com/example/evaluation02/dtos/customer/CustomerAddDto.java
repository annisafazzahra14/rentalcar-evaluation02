package com.example.evaluation02.dtos.customer;

import lombok.Data;

@Data
public class CustomerAddDto {
    private String name;
    private String address;
    private String gender;
    private String idCard;
    private Float fineBill = 0F;
    private String phone;
}
