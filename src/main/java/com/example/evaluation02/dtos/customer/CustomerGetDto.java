package com.example.evaluation02.dtos.customer;

import lombok.Data;

import java.util.UUID;

@Data
public class CustomerGetDto {
    private UUID id;
    private String name;
    private String address;
    private String gender;
    private String phone;
    private String idCard;
    private Float fineBill;
}
