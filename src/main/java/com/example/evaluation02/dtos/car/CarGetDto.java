package com.example.evaluation02.dtos.car;

import lombok.Data;

import java.util.UUID;

@Data
public class CarGetDto {
    private UUID id;
    private String brand;
    private String plate;
    private String color;
    private String status;
    private Integer year;
}
