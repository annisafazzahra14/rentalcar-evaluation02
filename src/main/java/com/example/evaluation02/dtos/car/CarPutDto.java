package com.example.evaluation02.dtos.car;

import lombok.Data;

@Data
public class CarPutDto {
    private String brand;
    private String plate;
    private String color;
    private Integer year;
}
