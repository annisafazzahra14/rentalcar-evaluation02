package com.example.evaluation02.dtos.car;

import lombok.Data;

import java.util.UUID;

@Data
public class CarAddDto {
    private String brand;
    private String plate;
    private String color;
    private String status = "not rented";
    private Integer year;
}
