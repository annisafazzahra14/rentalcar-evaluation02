package com.example.evaluation02.exception;

public class NonExistanceResourceException extends Exception{

    private String message;
    public NonExistanceResourceException(String message){
        super(message);
    }
}
