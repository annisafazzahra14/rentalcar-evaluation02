package com.example.evaluation02.exception;

public class ResourceAlreadyExistException extends Exception{
    public ResourceAlreadyExistException(String message){
        super(message);
    }
}
